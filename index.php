<?php include('connection.php'); ?>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$("#country").change(function(){
			var country = $(this).val();
			$.ajax({
				url:'countries.php',				
				data: {'country_id':country},
				success:function(result){										
					$('#state').html(result);
				}
			});
		});
		$("#state").change(function(){
			var state_id = $(this).val();
			$.ajax({
				url:'states.php',				
				data: {'state_id':state_id},
				success:function(result){										
					$('#city').html(result);
				}
			});
		});
	});
</script>
	<body style="text-align:center;">
		<h1>Basic form</h1>	
		
		<form>
			<select name="countries" id="country">
				<option value="0">--Select your country---</option>
			<?php 
				$sql = "select * from countries";
				$result = mysqli_query($conn,$sql);
				while($row = mysqli_fetch_array($result)){
					#print_r($row);
				?>
				<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>				
				<?php }
				?>
			</select>
			<select name="states" id="state">
				<option value="0">--Select your state---</option>
			<?php 
				$sql = "select * from states";
				$result = mysqli_query($conn,$sql);
				while($row = mysqli_fetch_array($result)){
					#print_r($row);
				?>
				<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>				
				<?php }
				?>
			</select>
			<select name="cities" id="city">
				<option value="0">--Select your city---</option>
			<?php 
				$sql = "select * from cities limit 100";
				$result = mysqli_query($conn,$sql);
				while($row = mysqli_fetch_array($result)){
					#print_r($row);
				?>
				<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>				
				<?php }
				?>
			</select>
		</form>
	</body>
</html>